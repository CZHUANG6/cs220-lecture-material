# Project 10 (P10) grading rubric

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Displaying excessive irrelevant information (-3)
- Required outputs not visible/did not save the notebook file prior to running the cell containing "export" (-3)
- Used concepts/modules (like csv.DictReader, pandas) not covered in class yet (built-in functions that you have been introduced to can be used) (-3)
- import statements are not mentioned in the required cell at the top of the notebook or used additional import statements beyond those that are stated in the directions (-3)
- Used bare try/except blocks (without explicitly specifying the type of exceptions that need to be caught) (-3)

### Question specific guidelines:

- Q1 (3)
	- Answer is not sorted (-2)
	- Answer does not remove all files and directories that start with "." (-1)

- Q2 (3)
	- Recomputed variable defined in Question 1/Answer is not sorted (-1)
	- Answer does not remove all files and directories that start with "." (-1)
	- Paths are hardcoded using "/" or "\\" (-1)

- Q3 (4)
	- Recomputed variable defined in Question 1 or Question 2/Answer is not sorted (-1)
	- Answer does not remove all files and directories that start with "." (-1)
	- Answer does not check for files that end with ‘.csv’ (-1)
	- Paths are hardcoded using "/" or "\\" (-1)

- Q4 (4)
	- Recomputed variable defined in Question 1 or Question 2/Answer is not sorted (-1)
	- Answer does not remove all files and directories that start with "." (-1)
	- Answer does not check for files that start with 'stars' (-1)
	- Paths are hardcoded using "/" or "\\" (-1)

- `Star` (2)
	- Data structure is defined incorrectly (-1)
	- Data structure is defined more than once (-1)

- `star_cell` (4)
	- Function logic is incorrect (-1)
	- Column indices are hardcoded (-1)
	- Function does not typecast based on column names (-1)
	- Function is defined more than once (-1)

- Q5 (4)
	- Required function is not used (-1)
	- Unnecessarily iterating over the entire dataset (-1)

- `get_stars` (6)
	- Function logic is incorrect (-2)
	- Required function is not used (-1)
	- Hardcoding the name of directory inside the function (-1)
	- Function is called more than twice with the same dataset (-1)
	- Function is defined more than once (-1)

- Q6 (2)
	- Required data structure is not used (-2)

- Q7 (3)
	- Required data structure is not used (-1)
	- Incorrect logic is used to answer (-1)

- Q8 (3)
	- Required function is not used (-1)
	- Incorrect logic is used to answer (-1)

- `stars_dict` (3)
	- Data structure is defined incorrectly (-1)
	- Required function is not used (-1)
	- Required data structure is not used (-1)

- Q9 (2)  
	- Required data structure is not used (-2)

- Q10 (3)
	- Required data structure is not used (-1)
	- Incorrect logic is used to answer (-1)

- Q11 (3)  
	- Required data structure is not used (-1)
	- Answer does not check for stars that start with 'Kepler' (-1)
	- Incorrect logic is used to answer (-1)

- `Planet` (2)
	- Data structure is defined incorrectly (-1)
	- Data structure is defined more than once (-1)

- `planet_cell` (5)
	- Function logic is incorrect (-1)
	- Column indices are hardcoded (-1)
	- Function does not typecast based on column names (-1)
	- Booleans are not typecasted correctly (-1)
	- Function is defined more than once (-1)

- Q12 (4)
	- Required function is not used (-1)
	- Required data structure is not used (-1)
	- Unnecessarily iterating over the entire dataset (-1)

- `get_planets` (8)
	- Function logic is incorrect (-2)
	- Required function is not used (-1)
	- Broken CSV rows are not skipped or are hardcoded (-1)
	- Broken JSON files are not skipped or are hardcoded (-1)
	- Hardcoding the name of directory inside the function (-1)
	- Function is called more than twice with the same dataset (-1)
	- Function is defined more than once (-1)

- Q13 (3)
	- Required function is not used (-1)
	- Paths are hardcoded using "/" or "\\" (-1)

- Q14 (3)
	- Required function is not used (-1)
	- Paths are hardcoded using "/" or "\\" (-1)
	- Incorrect logic is used to answer (-1)

- Q15 (3)
	- Required function is not used (-2)
	- Paths are hardcoded using "/" or "\\" (-1)

- `planets_list` (4)
	- Data structure is defined incorrectly (-2)
	- Required function is not used (-1)
	- Paths are hardcoded using "/" or "\\" (-1)

- Q16 (2)
	- Required data structure is not used (-2)

- Q17 (3)
	- Required data structure is not used (-1)
	- Incorrect comparison operator is used (-1)

- Q18 (4)
	- Required data structures are not used (-2)
	- Did not exit loop and instead iterated further after finding the answer (-1)

- Q19 (5)
	- Required data structures are not used (-2)
	- Incorrect comparison operator is used (-1)

- Q20 (5)
	- Required data structures are not used (-2)
	- Answer does not include all Planets (-1)
