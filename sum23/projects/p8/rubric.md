# Project 8 (P8) grading rubric

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Outputs not visible/did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-3)
- Used concepts/modules (ex: csv.DictReader, pandas) not covered in class yet (built-in functions that you have been introduced to can be used) (-3)
- Large outputs (such as `movies`) are displayed in the notebook (-3)
- import statements are not mentioned in the required cell at the top of the notebook or used additional import statements beyond those that are stated in the directions (-1)

### Question specific guidelines:

- `get_mapping` (3)
	- Incorrect logic is used in function (-2)
	- Function defined more than once (-1)

- Q1 (3)
	- Required function is not used to answer (-2)

- Q2 (3)
	- Required data structure is not used (-2)
	- Answer uses loops to iterate over dictionary keys (-1)

- Q3 (3)
	- Required data structure is not used (-2)
	- Answer does not check only for keys that begin with nm (-1)

- Q4 (4)
	- Required data structure is not used (-1)
	- Answer does not exclude people whose first name or middle name is Jones (-1)
	- Answer does not exclude people whose last name contains Jones as a substring (-1)
	- Answer does not exclude movie titles (-1)

- `get_raw_movies` (5)
	- Incorrect logic is used in function (-3)
	- Column indices are hardcoded instead of using the header to identify column names (-1)
	- Function defined more than once (-1)

- Q5 (3)
	- Required function is not used to answer (-2)

- Q6 (3)
	- Required data structure is not used (-1)
	- Answer uses loops to find number of cast members (-1)
	- Answer uses loops to iterate over dictionary keys (-1)

- Q7 (3)
	- Required data structure is not used (-1)
	- Answer uses loops to iterate over dictionary keys (-1)

- `get_movies` (8)
	- Function is called more than once with the same dataset (-4)
	- Incorrect logic is used in function (-2)
	- Function loops through IDs in the `mapping_path` file (-2)

- `small_movies_data` (3)
	- Data structure is defined incorrectly (-2)
	- Data structure is modified (-1)

- Q8 (3)
	- Required function is not used to answer (-2)

- Q9 (3)
	- Required data structure is not used (-2)
	- Answer uses loops to iterate over dictionary keys (-1)

- Q10 (3)
	- Required data structure is not used (-2)
	- Answer uses loops to iterate over dictionary keys (-1)

- Q11 (3)
	- Required data structure is not used (-1)
	- Answer uses loops to iterate over dictionary keys (-1)
	- Index is hardcoded (-1)

- `movies` (5)
	- Data structure is defined incorrectly (-3)
	- Data structure is modified (-2)

- Q12 (3)
	- Required data structure is not used (-2)

- Q13 (3)
	- Required data structure is not used (-1)
	- Incorrect logic is used to answer (-1)
	- Answer uses loops to iterate over dictionary keys (-1)

- `find_specific_movies` (2)
	- Function is redefined/ defined more than once (-2)

- Q14 (4)
	- Required function is not used to answer (-4)

- Q15 (4)
	- Required function is not used to answer (-4)

- `bucketize_by_genre` (4)
	- Incorrect logic is used in function (-2)
	- Function is called more than once with the same input list (-2)

- `genre_dict` (5)
	- Data structure is defined incorrectly (-3)
	- Required function is not used (-1)
	- Data structure is modified (-1)

- Q16 (3)
	- Required data structure is not used (-2)
	- Answer uses loops to find number of unique movie genres (-1)

- Q17 (3)
	- Required data structure is not used (-2)
	- Answer uses loops to iterate over dictionary keys (-1)

- Q18 (3)
	- Required data structure is not used (-1)
	- Answer uses loops to iterate over dictionary keys (-1)
	- Correct comparison operator is not used (-1)

- Q19 (4)
	- Incorrect logic is used to answer (-2)

- Q20 (4)
	- Incorrect logic is used to answer (-2)
